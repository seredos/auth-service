#!/bin/bash

if [[ ${1:-} = "" ]]
then
  php bin/console cache:clear
  php bin/console doctrine:database:create --if-not-exists
  php bin/console doctrine:schema:update --force --no-interaction
  php-fpm &>/dev/null &
  php bin/console stream:consume
else
    exec "$@"
fi
