<?php


namespace StreamBundle\Service;


interface StreamConsumerInterface {
    public function getTopic(): string;

    public function consume(array $data): void;
}
