<?php


namespace StreamBundle\Service;


abstract class BaseKafka {
    protected $kafkaConfig;

    public function __construct () {
        $this->kafkaConfig = new \RdKafka\Conf();
        $this->kafkaConfig->set('log_level', (string) LOG_ERR);
    }
}
