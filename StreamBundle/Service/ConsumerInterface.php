<?php


namespace StreamBundle\Service;


interface ConsumerInterface {
    public function addConsumer(StreamConsumerInterface $consumer): void;

    public function consume(): void;
}
