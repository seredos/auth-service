<?php


namespace StreamBundle\Service;


class Config {
    private $brokers;

    public function __construct(){
        $this->brokers = [];
    }

    public function addBroker(string $broker): void {
        $this->brokers[] = $broker;
    }

    public function getBrokers(): array {
        return $this->brokers;
    }
}
