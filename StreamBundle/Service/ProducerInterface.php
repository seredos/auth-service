<?php


namespace StreamBundle\Service;


interface ProducerInterface {
    public function produce (string $topic, $data): bool;
}
