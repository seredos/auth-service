<?php


namespace StreamBundle\Service;


use JMS\Serializer\SerializerInterface;

class KafkaConsumer extends BaseKafka implements ConsumerInterface {
    private $consumer;
    private $serializer;
    private $consumerId;
    private $consumers;

    public function __construct (SerializerInterface $serializer, $consumerId, array $brokers) {
        parent::__construct();
        $this->serializer = $serializer;
        $this->consumerId = $consumerId;
        $this->consumers = [];

        $this->consumer = new \RdKafka\Consumer($this->kafkaConfig);
        $this->consumer->addBrokers(implode(',', $brokers));
    }

    public function addConsumer (StreamConsumerInterface $consumer): void {
        $this->consumers[$consumer->getTopic()][] = $consumer;
    }

    public function consume (): void {
        $queue = $this->consumer->newQueue();

        foreach ($this->consumers as $topic => $consumer) {
            $tp = $this->consumer->newTopic($topic);
            $tp->consumeQueueStart(0, RD_KAFKA_OFFSET_BEGINNING, $queue);
        }

        while (true) {
            $msg = $queue->consume(1000);
            if ($msg !== null && $msg->err === 0) {
                foreach ($this->consumers[$msg->topic_name] as $consumer) {
                    $payload = $this->serializer->deserialize($msg->payload, 'array', 'json');
                    if (isset($payload['consumer_id'], $payload['data'])) {
                        if ($payload['consumer_id'] !== $this->consumerId) {
                            $consumer->consume($payload['data']);
                        }
                    }
                }
            }
        }
    }
}
