<?php


namespace StreamBundle\Service;


use JMS\Serializer\SerializerInterface;

class KafkaProducer extends BaseKafka implements ProducerInterface {
    private $producer;
    private $serializer;
    private $consumerId;

    public function __construct (SerializerInterface $serializer, $consumerId, array $brokers) {
        parent::__construct();
        $this->serializer = $serializer;
        $this->consumerId = $consumerId;

        $this->producer = new \RdKafka\Producer($this->kafkaConfig);
        $this->producer->addBrokers(implode(',', $brokers));
    }

    public function produce (string $topic, $data): bool {
        $tp = $this->producer->newTopic($topic);
        $content = ['consumer_id' => $this->consumerId, 'data' => $data];
        $tp->produce(RD_KAFKA_PARTITION_UA, 0, $this->serializer->serialize($content, 'json'));
        return true;
    }
}
