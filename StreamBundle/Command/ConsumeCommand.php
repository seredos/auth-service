<?php


namespace StreamBundle\Command;


use StreamBundle\Service\ConsumerInterface;
use StreamBundle\Service\StreamConsumerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class ConsumeCommand extends Command {
    protected static $defaultName = 'stream:consume';
    private $consumer;
//    private $adapter;
//
//    public function __construct (StreamAdapter $adapter) {
//        parent::__construct($this::$defaultName);
//        $this->adapter = $adapter;
//    }

    /**
     * ConsumeCommand constructor.
     *
     * @param ConsumerInterface         $consumer
     * @param StreamConsumerInterface[] $consumers
     */
    public function __construct (ConsumerInterface $consumer, array $consumers) {
        parent::__construct($this::$defaultName);
        $this->consumer = $consumer;

        foreach($consumers as $currentConsumer){
            $this->consumer->addConsumer($currentConsumer);
        }
    }

    protected function configure()
    {
        $this
            ->setDescription('Add a short description for your command')
            //->addArgument('arg1', InputArgument::OPTIONAL, 'Argument description')
            //->addOption('option1', null, InputOption::VALUE_NONE, 'Option description')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

//        $this->adapter->consume(['user']);

        $this->consumer->consume();
        $io->success('You have a new command! Now make it your own! Pass --help to see your options.');

        return 0;
    }
}
