<?php


namespace StreamBundle\DependencyInjection;


use StreamBundle\Command\ConsumeCommand;
use StreamBundle\Service\Config;
use StreamBundle\Service\ConsumerInterface;
use StreamBundle\Service\KafkaConsumer;
use StreamBundle\Service\KafkaProducer;
use StreamBundle\Service\ProducerInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class StreamExtension extends Extension implements CompilerPassInterface {
    private $config;

    public function load (array $configs, ContainerBuilder $container) {
        $configuration = new Configuration();

        $this->config = $this->processConfiguration($configuration, $configs);
    }

    public function process (ContainerBuilder $container) {
        $container->register(ProducerInterface::class, KafkaProducer::class)
                  ->addArgument($container->findDefinition('jms_serializer'))
                  ->addArgument($this->config['consumer_id'])
                  ->addArgument($this->config['brokers']);

        $services = $container->findTaggedServiceIds('stream.consumer');
        $consumerDefinition = $container->register(ConsumerInterface::class, KafkaConsumer::class)
                                        ->addArgument($container->findDefinition('jms_serializer'))
                                        ->addArgument($this->config['consumer_id'])
                                        ->addArgument($this->config['brokers']);

        $consumers = [];
        foreach ($services as $key => $service) {
            $consumers[] = $container->findDefinition($key);
        }

        $container->register(ConsumeCommand::class, ConsumeCommand::class)
                  ->addArgument($consumerDefinition)
                  ->addArgument($consumers)
                  ->addTag('console.command', ['command' => 'stream:consume']);
    }
}
