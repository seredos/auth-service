<?php


namespace StreamBundle\DependencyInjection;


use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface {

    public function getConfigTreeBuilder () {
        $treeBuilder = new TreeBuilder('stream');

        $treeBuilder->getRootNode()
                    ->children()
                    ->scalarNode('consumer_id')
                    ->isRequired()
                    ->end()
                    ->arrayNode('brokers')
                    ->scalarPrototype()
                    ->end()
                    ->isRequired()
                    ->end()
                    ->end();

        return $treeBuilder;
    }
}
