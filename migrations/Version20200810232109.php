<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200810232109 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE client (id INT AUTO_INCREMENT NOT NULL, identifier VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, redirect_uri VARCHAR(255) DEFAULT NULL, confidential TINYINT(1) NOT NULL, secret VARCHAR(255) DEFAULT NULL, UNIQUE INDEX UNIQ_C7440455772E836A (identifier), UNIQUE INDEX UNIQ_C74404555E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_57698A6A5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE role_scope (role_id INT NOT NULL, scope_id INT NOT NULL, INDEX IDX_F773BDFCD60322AC (role_id), INDEX IDX_F773BDFC682B5931 (scope_id), PRIMARY KEY(role_id, scope_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE scope (id INT AUTO_INCREMENT NOT NULL, identifier VARCHAR(255) NOT NULL, untrusted TINYINT(1) NOT NULL, UNIQUE INDEX UNIQ_AF55D3772E836A (identifier), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, email VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, firstname VARCHAR(255) NOT NULL, lastname VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649E7927C74 (email), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user_role (user_id INT NOT NULL, role_id INT NOT NULL, INDEX IDX_2DE8C6A3A76ED395 (user_id), INDEX IDX_2DE8C6A3D60322AC (role_id), PRIMARY KEY(user_id, role_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE role_scope ADD CONSTRAINT FK_F773BDFCD60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE role_scope ADD CONSTRAINT FK_F773BDFC682B5931 FOREIGN KEY (scope_id) REFERENCES scope (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_role ADD CONSTRAINT FK_2DE8C6A3D60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');

        $this->addSql('INSERT INTO role(name) VALUES("guest")');
        $this->addSql('INSERT INTO role(name) VALUES("admin")');

        $this->addSql('INSERT INTO scope(identifier, untrusted) VALUES("credentials:read", true)');
        $this->addSql('INSERT INTO scope(identifier, untrusted) VALUES("credentials:write", false)');

        $this->addSql('INSERT INTO role_scope(role_id,scope_id)
SELECT r.id, s.id FROM role r
LEFT JOIN scope s ON s.identifier = "credentials:read"
WHERE r.name = "guest"');

        $this->addSql('INSERT INTO role_scope(role_id,scope_id)
SELECT r.id, s.id FROM role r
LEFT JOIN scope s ON s.identifier = "credentials:write"
WHERE r.name = "guest"');

        $this->addSql('INSERT INTO role_scope(role_id,scope_id)
SELECT r.id, s.id FROM role r
LEFT JOIN scope s ON s.identifier = "credentials:read"
WHERE r.name = "admin"');

        $this->addSql('INSERT INTO role_scope(role_id,scope_id)
SELECT r.id, s.id FROM role r
LEFT JOIN scope s ON s.identifier = "credentials:write"
WHERE r.name = "admin"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE role_scope DROP FOREIGN KEY FK_F773BDFCD60322AC');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3D60322AC');
        $this->addSql('ALTER TABLE role_scope DROP FOREIGN KEY FK_F773BDFC682B5931');
        $this->addSql('ALTER TABLE user_role DROP FOREIGN KEY FK_2DE8C6A3A76ED395');
        $this->addSql('DROP TABLE client');
        $this->addSql('DROP TABLE role');
        $this->addSql('DROP TABLE role_scope');
        $this->addSql('DROP TABLE scope');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE user_role');
    }
}
