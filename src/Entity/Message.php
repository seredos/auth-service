<?php


namespace App\Entity;


use JMS\Serializer\Annotation as Serializer;

/**
 * Class Message
 * @package App\Entity
 * @Serializer\XmlRoot("message")
 */
class Message {
    /**
     * @var int
     */
    private $code = 500;
    /**
     * @var string
     */
    private $key = 'unknown';
    /**
     * @var string
     */
    private $message = 'unknown error';

    /**
     * @return int
     */
    public function getCode (): int {
        return $this->code;
    }

    /**
     * @param int $code
     *
     * @return Message
     */
    public function setCode (int $code): Message {
        $this->code = $code;

        return $this;
    }

    /**
     * @return string
     */
    public function getKey (): string {
        return $this->key;
    }

    /**
     * @param string $key
     *
     * @return Message
     */
    public function setKey (string $key): Message {
        $this->key = $key;

        return $this;
    }

    /**
     * @return string
     */
    public function getMessage (): string {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return Message
     */
    public function setMessage (string $message): Message {
        $this->message = $message;

        return $this;
    }
}
