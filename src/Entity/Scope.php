<?php


namespace App\Entity;


use App\Repository\ScopeRepository;
use Doctrine\ORM\Mapping as ORM;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Entities\Traits\ScopeTrait;

/**
 * @ORM\Entity(repositoryClass=ScopeRepository::class)
 */
class Scope implements ScopeEntityInterface {
    use ScopeTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @var int|null
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true)
     */
    protected $identifier;

    /**
     * @ORM\Column(type="boolean")
     */
    private $untrusted;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getIdentifier (): ?string {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return Scope
     */
    public function setIdentifier (string $identifier): Scope {
        $this->identifier = $identifier;

        return $this;
    }

    public function getUntrusted(): ?bool
    {
        return $this->untrusted;
    }

    public function setUntrusted(bool $untrusted): self
    {
        $this->untrusted = $untrusted;

        return $this;
    }
}
