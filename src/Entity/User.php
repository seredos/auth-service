<?php

namespace App\Entity;

use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use League\OAuth2\Server\Entities\UserEntityInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserEntityInterface {
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $password;
    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $firstname;
    /**
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $lastname;

    /**
     * @ORM\ManyToMany(targetEntity=Role::class)
     */
    private $roles;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $googleId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $google_token;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $facebookToken;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $githubId;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $githubToken;

    public function __construct () {
        $this->roles = new ArrayCollection();
    }

    public function getId (): ?int {
        return $this->id;
    }

    public function getEmail (): ?string {
        return $this->email;
    }

    public function setEmail (string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getPassword (): ?string {
        return $this->password;
    }

    public function setPassword (string $password): self {
        $this->password = $password;

        return $this;
    }

    public function savePassword (string $password): self {
        $this->password = password_hash($password, PASSWORD_BCRYPT);

        return $this;
    }

    /**
     * @return string
     */
    public function getFirstname (): ?string {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname ($firstname): self {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * @return string
     */
    public function getLastname (): ?string {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname ($lastname): self {
        $this->lastname = $lastname;

        return $this;
    }

    public function getIdentifier () {
        return $this->getId();
    }

    /**
     * @return Collection|Role[]
     */
    public function getRoles (): Collection {
        return $this->roles;
    }

    public function addRole (Role $role): self {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    public function removeRole (Role $role): self {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
        }

        return $this;
    }

    public function getGoogleId (): ?string {
        return $this->googleId;
    }

    public function setGoogleId (?string $googleId): self {
        $this->googleId = $googleId;

        return $this;
    }

    public function getGoogleToken (): ?string {
        return $this->google_token;
    }

    public function setGoogleToken (?string $google_token): self {
        $this->google_token = $google_token;

        return $this;
    }

    public function getFacebookId(): ?string
    {
        return $this->facebookId;
    }

    public function setFacebookId(?string $facebookId): self
    {
        $this->facebookId = $facebookId;

        return $this;
    }

    public function getFacebookToken(): ?string
    {
        return $this->facebookToken;
    }

    public function setFacebookToken(?string $facebookToken): self
    {
        $this->facebookToken = $facebookToken;

        return $this;
    }

    public function getGithubId(): ?string
    {
        return $this->githubId;
    }

    public function setGithubId(?string $githubId): self
    {
        $this->githubId = $githubId;

        return $this;
    }

    public function getGithubToken(): ?string
    {
        return $this->githubToken;
    }

    public function setGithubToken(?string $githubToken): self
    {
        $this->githubToken = $githubToken;

        return $this;
    }
}
