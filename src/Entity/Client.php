<?php


namespace App\Entity;


use App\Repository\ClientRepository;
use Doctrine\ORM\Mapping as ORM;
use League\OAuth2\Server\Entities\ClientEntityInterface;

/**
 * @ORM\Entity(repositoryClass=ClientRepository::class)
 */
class Client implements ClientEntityInterface {
    /**
     * @var int
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true, nullable=false)
     */
    private $identifier;
    /**
     * @var string
     * @ORM\Column(type="string", length=255, unique=true,nullable=false)
     */
    private $name;
    /**
     * @var string
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $redirectUri;
    /**
     * @var boolean
     * @ORM\Column(type="boolean")
     */
    private $confidential = false;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255,nullable=true)
     */
    private $secret;

    /**
     * @return string
     */
    public function getIdentifier (): string {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return Client
     */
    public function setIdentifier (string $identifier): Client {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getName (): string {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Client
     */
    public function setName (string $name): Client {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getRedirectUri (): ?string {
        return $this->redirectUri;
    }

    /**
     * @param string $redirectUri
     *
     * @return Client
     */
    public function setRedirectUri (string $redirectUri = null): Client {
        $this->redirectUri = $redirectUri;

        return $this;
    }

    /**
     * @return bool
     */
    public function isConfidential (): bool {
        return $this->confidential;
    }

    /**
     * @param bool $confidential
     *
     * @return Client
     */
    public function setConfidential (bool $confidential): Client {
        $this->confidential = $confidential;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId (): ?int {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getSecret (): ?string {
        return $this->secret;
    }

    /**
     * @param string $secret
     *
     * @return Client
     */
    public function setSecret (string $secret): Client {
        $this->secret = $secret;

        return $this;
    }

}
