<?php


namespace App\Humble;


use App\Entity\User;
use App\Repository\UserRepository;
use Exception;
use League\OAuth2\Client\Provider\Google;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class GoogleAuthorizationHumble extends AbstractProviderAuthorizationHumble {
    protected const REDIRECT_URI = 'google_default_redirect';

    /**
     * GoogleAuthorizationHumble constructor.
     *
     * @param AuthorizationServer             $server
     * @param PsrHttpFactory                  $httpFactory
     * @param HttpFoundationFactory           $symfonyFactory
     * @param AuthCodeRepositoryInterface     $authCodeRepository
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     * @param UrlGeneratorInterface           $urlGenerator
     * @param UserRepository                  $userRepository
     * @param string                          $googleClientId
     * @param string                          $googleClientSecret
     *
     * @throws Exception
     */
    public function __construct (AuthorizationServer $server,
                                 PsrHttpFactory $httpFactory,
                                 HttpFoundationFactory $symfonyFactory,
                                 AuthCodeRepositoryInterface $authCodeRepository,
                                 RefreshTokenRepositoryInterface $refreshTokenRepository,
                                 UrlGeneratorInterface $urlGenerator,
                                 UserRepository $userRepository,
                                 string $googleClientId,
                                 string $googleClientSecret) {
        $provider = new Google(['clientId' => $googleClientId,
                                      'clientSecret' => $googleClientSecret,
                                      'redirectUri' => $urlGenerator->generate($this::REDIRECT_URI,
                                                                               [],
                                                                               UrlGeneratorInterface::ABSOLUTE_URL),
                                      'state' => '']);
        parent::__construct($server,
                            $httpFactory,
                            $symfonyFactory,
                            $authCodeRepository,
                            $refreshTokenRepository,
                            $urlGenerator,
                            $userRepository,
                            $provider);
    }

    protected function findUser ($owner, $token): ?User {
        $user = $this->userRepository->findOneBy(['googleId' => $owner->getId()]);

        if($user === null){
            $user = parent::findUser($owner, $token);
        }

        if($user !== null){
            $user->setGoogleToken($token);
            $user->setGoogleId($owner->getId());
        }
        return $user;
    }

    protected function createUser ($owner, $token): User {
        $user = parent::createUser($owner, $token);
        $user->setGoogleToken($token);
        $user->setGoogleId($owner->getId());
        return $user;
    }
}
