<?php


namespace App\Humble;


use App\Entity\User;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

interface AuthorizationHumbleInterface {
    public function validate (Request $request): AuthorizationRequest;

    public function response (Request $request): Response;

    public function complete (AuthorizationRequest $authRequest, User $user = null): Response;
}
