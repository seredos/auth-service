<?php


namespace App\Humble;


use Exception;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;

class AuthorizationHumble extends AbstractAuthorizationHumble {
    /**
     * AuthorizationHumble constructor.
     *
     * @param AuthorizationServer             $server
     * @param AuthCodeRepositoryInterface     $authCodeRepository
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     * @param UserRepositoryInterface         $userRepository
     *
     * @param PsrHttpFactory                  $httpFactory
     *
     * @param HttpFoundationFactory           $symfonyFactory
     *
     * @throws Exception
     */
    public function __construct (AuthorizationServer $server,
                                 AuthCodeRepositoryInterface $authCodeRepository,
                                 RefreshTokenRepositoryInterface $refreshTokenRepository,
                                 UserRepositoryInterface $userRepository,
                                 PsrHttpFactory $httpFactory,
                                 HttpFoundationFactory $symfonyFactory) {
        parent::__construct($server, $httpFactory, $symfonyFactory);

        $this->registerAuthGrant($authCodeRepository, $refreshTokenRepository);
        $this->registerClientCredentials();
        $this->registerPassword($userRepository, $refreshTokenRepository);
        $this->registerRefreshToken($refreshTokenRepository);
    }
}
