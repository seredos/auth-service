<?php


namespace App\Humble;


use App\Entity\User;
use App\Repository\UserRepository;
use Exception;
use League\OAuth2\Client\Provider\Github;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class GithubAuthorizationHumble extends AbstractProviderAuthorizationHumble {
    protected const REDIRECT_URI = 'github_default_redirect';

    /**
     * GoogleAuthorizationHumble constructor.
     *
     * @param AuthorizationServer             $server
     * @param PsrHttpFactory                  $httpFactory
     * @param HttpFoundationFactory           $symfonyFactory
     * @param AuthCodeRepositoryInterface     $authCodeRepository
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     * @param UrlGeneratorInterface           $urlGenerator
     * @param UserRepository                  $userRepository
     * @param string                          $githubClientId
     * @param string                          $githubClientSecret
     *
     * @throws Exception
     */
    public function __construct (AuthorizationServer $server,
                                 PsrHttpFactory $httpFactory,
                                 HttpFoundationFactory $symfonyFactory,
                                 AuthCodeRepositoryInterface $authCodeRepository,
                                 RefreshTokenRepositoryInterface $refreshTokenRepository,
                                 UrlGeneratorInterface $urlGenerator,
                                 UserRepository $userRepository,
                                 string $githubClientId,
                                 string $githubClientSecret) {
        $provider = new Github(['clientId' => $githubClientId,
                                'clientSecret' => $githubClientSecret,
                                'redirectUri' => $urlGenerator->generate($this::REDIRECT_URI,
                                                                         [],
                                                                         UrlGeneratorInterface::ABSOLUTE_URL),
                                'state' => '']);
        parent::__construct($server,
                            $httpFactory,
                            $symfonyFactory,
                            $authCodeRepository,
                            $refreshTokenRepository,
                            $urlGenerator,
                            $userRepository,
                            $provider);
    }

    protected function findUser ($owner, $token): ?User {
        $user = $this->userRepository->findOneBy(['githubId' => $owner->getId()]);

        if($user === null){
            $user = parent::findUser($owner, $token);
        }

        if($user !== null){
            $user->setGithubToken($token);
            $user->setGithubId($owner->getId());
        }
        return $user;
    }

    protected function createUser ($owner, $token): User {
        $name = $owner->getName();
        $firstName = explode(' ',$name)[0];
        $names = explode(' ',$name);
        array_shift($names);
        $lastName = implode(' ', $names);

        $user = new User();
        $user->setGithubId($owner->getId())
             ->setLastname($lastName)
             ->setFirstname($firstName)
             ->setEmail($owner->getEmail());

        $user->setGithubToken($token);
        $user->setGithubId($owner->getId());
        return $user;
    }
}
