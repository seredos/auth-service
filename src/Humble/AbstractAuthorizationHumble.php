<?php


namespace App\Humble;


use App\Entity\User;
use DateInterval;
use Exception;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AuthCodeGrant;
use League\OAuth2\Server\Grant\ClientCredentialsGrant;
use League\OAuth2\Server\Grant\PasswordGrant;
use League\OAuth2\Server\Grant\RefreshTokenGrant;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractAuthorizationHumble implements AuthorizationHumbleInterface {
    /**
     * @var AuthorizationServer
     */
    protected $server;
    /**
     * @var PsrHttpFactory
     */
    private $httpFactory;
    /**
     * @var HttpFoundationFactory
     */
    private $symfonyFactory;

    /**
     * AbstractAuthorizationHumble constructor.
     *
     * @param AuthorizationServer   $server
     * @param PsrHttpFactory        $httpFactory
     * @param HttpFoundationFactory $symfonyFactory
     */
    public function __construct (AuthorizationServer $server,
                                 PsrHttpFactory $httpFactory,
                                 HttpFoundationFactory $symfonyFactory) {
        $this->server = $server;
        $this->httpFactory = $httpFactory;
        $this->symfonyFactory = $symfonyFactory;
    }


    /**
     * @param Request $request
     *
     * @return AuthorizationRequest
     * @throws OAuthServerException
     */
    public function validate (Request $request): AuthorizationRequest {
        $psrRequest = $this->httpFactory->createRequest($request);

        return $this->server->validateAuthorizationRequest($psrRequest);
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws OAuthServerException
     */
    public function response (Request $request): Response {
        $psrRequest = $this->httpFactory->createRequest($request);
        $psrResponse = $this->httpFactory->createResponse(new Response());
        $psrResponse = $this->server->respondToAccessTokenRequest($psrRequest, $psrResponse);

        return $this->symfonyFactory->createResponse($psrResponse);
    }

    /**
     * @param AuthorizationRequest $authRequest
     * @param User|null            $user
     *
     * @return Response
     */
    public function complete (AuthorizationRequest $authRequest, User $user = null): Response {
        if ($user) {
            $authRequest->setUser($user);
        }
        $authRequest->setAuthorizationApproved(true);
        $psrResponse = $this->httpFactory->createResponse(new Response());
        $psrResponse = $this->server->completeAuthorizationRequest($authRequest, $psrResponse);

        return $this->symfonyFactory->createResponse($psrResponse);
    }

    /**
     * @param AuthCodeRepositoryInterface     $authCodeRepository
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     *
     * @throws Exception
     */
    protected function registerAuthGrant (AuthCodeRepositoryInterface $authCodeRepository,
                                          RefreshTokenRepositoryInterface $refreshTokenRepository): void {
        $authCodeGrant = new AuthCodeGrant(
            $authCodeRepository,
            $refreshTokenRepository,
            new DateInterval('PT10M') // authorization codes will expire after 10 minutes
        );

        $authCodeGrant->setRefreshTokenTTL(new DateInterval('P1M'));

        $this->server->enableGrantType(
            $authCodeGrant,
            new DateInterval('PT1H') // access tokens will expire after 1 hour
        );
    }

    protected function registerClientCredentials (): void {
        $this->server->enableGrantType(
            new ClientCredentialsGrant(),
            new DateInterval('PT1H') // access tokens will expire after 1 hour
        );
    }

    /**
     * @param UserRepositoryInterface         $userRepository
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     */
    protected function registerPassword (UserRepositoryInterface $userRepository,
                                         RefreshTokenRepositoryInterface $refreshTokenRepository): void {
        $passwordGrant = new PasswordGrant(
            $userRepository,
            $refreshTokenRepository
        );

        $passwordGrant->setRefreshTokenTTL(new DateInterval('P1M'));

        $this->server->enableGrantType(
            $passwordGrant,
            new DateInterval('PT1H') // access tokens will expire after 1 hour
        );
    }

    /**
     * @param RefreshTokenRepositoryInterface $refreshTokenRepository
     */
    protected function registerRefreshToken (RefreshTokenRepositoryInterface $refreshTokenRepository): void {
        $refreshGrant = new RefreshTokenGrant($refreshTokenRepository);
        $refreshGrant->setRefreshTokenTTL(new DateInterval('P1M'));

        $this->server->enableGrantType(
            $refreshGrant,
            new DateInterval('PT1H') // new access tokens will expire after an hour
        );
    }
}
