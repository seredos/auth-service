<?php


namespace App\Humble;


use App\Entity\User;
use App\Repository\UserRepository;
use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Server\AuthorizationServer;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\RequestTypes\AuthorizationRequest;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class AbstractProviderAuthorizationHumble extends AbstractAuthorizationHumble {
    protected const REDIRECT_URI = '';

    /**
     * @var AbstractProvider
     */
    protected $provider;
    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;
    /**
     * @var UserRepository
     */
    protected $userRepository;

    public function __construct (AuthorizationServer $server,
                                 PsrHttpFactory $httpFactory,
                                 HttpFoundationFactory $symfonyFactory,
                                 AuthCodeRepositoryInterface $authCodeRepository,
                                 RefreshTokenRepositoryInterface $refreshTokenRepository,
                                 UrlGeneratorInterface $urlGenerator,
                                 UserRepository $userRepository,
                                 AbstractProvider $provider) {
        parent::__construct($server, $httpFactory, $symfonyFactory);
        $this->urlGenerator = $urlGenerator;
        $this->provider = $provider;

        $this->userRepository = $userRepository;

        $this->registerAuthGrant($authCodeRepository, $refreshTokenRepository);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function response (Request $request): Response {
        $url = $this->provider->getAuthorizationUrl();

        return new RedirectResponse($url);
    }

    protected function findUser ($owner, $token): ?User {
        return $this->userRepository->findOneBy(['email' => $owner->getEmail()]);
    }

    protected function createUser ($owner, $token): User {
        $user = new User();
        $user->setLastname($owner->getLastName())
             ->setFirstname($owner->getFirstName())
             ->setEmail($owner->getEmail());

        return $user;
    }

    public function validate (Request $request): AuthorizationRequest {
        $accessToken = $this->provider->getAccessToken('authorization_code',
                                                       [
                                                           'code' => $request->query->get('code')
                                                           ,
                                                           'redirect_uri' => $this->urlGenerator->generate($this::REDIRECT_URI,
                                                                                                           [],
                                                                                                           UrlGeneratorInterface::ABSOLUTE_URL)
                                                       ]);

        $owner = $this->provider->getResourceOwner($accessToken);

        $user = $this->findUser($owner, $accessToken->getToken());
        if ($user === null) {
            $user = $this->createUser($owner, $accessToken->getToken());
        }
        $user = $this->userRepository->createUser($user);

        $authRequest = parent::validate($request);
        $authRequest->setUser($user);

        return $authRequest;
    }
}
