<?php


namespace App\Listener;


use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;
use StreamBundle\Service\ProducerInterface;

class UserPersistListener implements EventSubscriber {
    private $stream;

    public function __construct (ProducerInterface $stream) {
        $this->stream = $stream;
    }

    public function postPersist (LifecycleEventArgs $args) {
        $entity = $args->getObject();

        $this->stream->produce('user', $entity);
    }

    public function getSubscribedEvents () {
        return [Events::postPersist];
    }
}
