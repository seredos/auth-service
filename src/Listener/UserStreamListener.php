<?php


namespace App\Listener;


use StreamBundle\Service\StreamConsumerInterface;

class UserStreamListener implements StreamConsumerInterface {

    public function getTopic (): string {
        return 'user';
    }

    public function consume(array $data): void {
        var_dump($data);
    }
}
