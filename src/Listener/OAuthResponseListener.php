<?php


namespace App\Listener;


use App\Entity\Message;
use App\Service\SerializationHandlerInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use Symfony\Bridge\PsrHttpMessage\Factory\HttpFoundationFactory;
use Symfony\Bridge\PsrHttpMessage\Factory\PsrHttpFactory;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class OAuthResponseListener implements EventSubscriberInterface {
    private $handler;
    private $httpFactory;
    private $symfonyFactory;

    public function __construct (SerializationHandlerInterface $handler,
                                 PsrHttpFactory $httpFactory,
                                 HttpFoundationFactory $symfonyFactory) {
        $this->handler = $handler;
        $this->httpFactory = $httpFactory;
        $this->symfonyFactory = $symfonyFactory;
    }

    public function onKernelResponse (ResponseEvent $event): void {
        $response = $event->getResponse();
        $response->headers->set('Access-Control-Allow-Origin', '*');
    }

    public function onKernelException (ExceptionEvent $event): void {
        $exception = $event->getThrowable();

        if (!$exception instanceof OAuthServerException) {
            return;
        }

        $psrRequest = $this->httpFactory->createRequest($event->getRequest());

        $message = new Message();
        $message->setCode($exception->getHttpStatusCode())
                ->setKey($exception->getCode())
                ->setMessage($exception->getMessage());

        $psrResponse = $this->handler->serialize($psrRequest, $message, $exception->getHttpStatusCode());
        if ($psrResponse) {
            $response = $this->symfonyFactory->createResponse($psrResponse);
            $response->setStatusCode($exception->getHttpStatusCode());
            $event->setResponse($response);
        }
    }

    public static function getSubscribedEvents (): array {
        return [
            KernelEvents::RESPONSE => ['onKernelResponse', 10],
            KernelEvents::EXCEPTION => ['onKernelException'],
        ];
    }
}
