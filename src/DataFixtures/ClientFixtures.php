<?php

namespace App\DataFixtures;

use App\Entity\Client;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ClientFixtures extends Fixture {
    public function load (ObjectManager $manager) {
        $this->clearUntrusted($manager);
        $this->createTrusted($manager);
        $this->createTrustedWithSecret($manager);

        $manager->flush();
    }

    private function clearUntrusted (ObjectManager $manager) {
        $client = $manager->getRepository(Client::class)
                          ->findOneBy(['identifier' => 'untrusted']);
        if ($client) {
            $manager->remove($client);
        }
    }

    private function createTrusted (ObjectManager $manager) {
        $client = $manager->getRepository(Client::class)
                          ->findOneBy(['identifier' => 'trusted']);
        if (!$client) {
            $client = new Client();
        }
        $client->setIdentifier('trusted')
               ->setName('trusted')
               ->setConfidential(true);
        $manager->persist($client);
    }

    private function createTrustedWithSecret (ObjectManager $manager) {
        $client = $manager->getRepository(Client::class)
                          ->findOneBy(['identifier' => 'trustedWithSecret']);
        if (!$client) {
            $client = new Client();
        }
        $client->setIdentifier('trustedWithSecret')
               ->setName('trustedWithSecret')
               ->setConfidential(true)
               ->setSecret('secretSecret');
        $manager->persist($client);
    }
}
