<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture {
    public function load (ObjectManager $manager) {
        $user = $manager->getRepository(User::class)
                        ->findOneBy(['email' => 'test@web.de']);
        if (!$user) {
            $user = new User();
        }

        $user->setEmail('test@web.de')
             ->setFirstname('Max')
             ->setLastname('Mustermann')
             ->setPassword(password_hash('test', PASSWORD_BCRYPT));

        $manager->persist($user);
        $manager->flush();
    }
}
