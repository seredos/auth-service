<?php


namespace App\Controller;


use App\Humble\GithubAuthorizationHumble;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GithubController extends AbstractProviderController {
    public function __construct (SessionInterface $session,
                                 GithubAuthorizationHumble $humble) {
        parent::__construct($session, $humble);
    }
}
