<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class RedirectController extends AbstractController {
    /**
     * @param Request $request
     *
     * @return Response
     */
    public function language (Request $request): Response {
        $browserLocale = strtolower(str_split($request->server->get('HTTP_ACCEPT_LANGUAGE'), 2)[0]);

        $params = $request->query->all();
        $params['_locale'] = $browserLocale;

        return $this->redirect($this->generateUrl('code', $params));
    }
}
