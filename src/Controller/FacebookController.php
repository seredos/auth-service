<?php


namespace App\Controller;


use App\Humble\FacebookAuthorizationHumble;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class FacebookController extends AbstractProviderController {
    public function __construct (SessionInterface $session,
                                 FacebookAuthorizationHumble $humble) {
        parent::__construct($session, $humble);
    }
}
