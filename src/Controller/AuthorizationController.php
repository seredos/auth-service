<?php


namespace App\Controller;


use App\Entity\LoginUser;
use App\Entity\User;
use App\Form\LoginForm;
use App\Form\RegistrationForm;
use App\Humble\AuthorizationHumble;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuthorizationController extends AbstractController {
    /**
     * @var UserRepositoryInterface
     */
    private $userRepository;
    /**
     * @var AuthorizationHumble
     */
    private $humble;

    /**
     * AuthorizationController constructor.
     *
     * @param AuthorizationHumble     $humble
     * @param UserRepositoryInterface $userRepository
     */
    public function __construct (AuthorizationHumble $humble, UserRepositoryInterface $userRepository) {
        $this->userRepository = $userRepository;
        $this->humble = $humble;
    }

    /**
     * @param Request $request
     *
     * @return Response
     * @throws OAuthServerException
     */
    public function authorization (Request $request): Response {
        if ($request->query->get('response_type') === 'code') {
            return $this->handleLogin($request);
        }

        return $this->humble->response($request);
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function visualize (Request $request): Response {
        return $this->render('redirect.html.twig', ['code' => $request->query->get('code')]);
    }

    /**
     * @param FormInterface $loginForm
     *
     * @return User|null
     */
    private function login (FormInterface $loginForm): ?User {
        $user = null;
        if ($loginForm->isSubmitted() && $loginForm->isValid()) {
            /**@var $loginUser LoginUser */
            $loginUser = $loginForm->getData();

            $user = $this->userRepository->getUserEntityByCredentials($loginUser->getEmail(),
                                                                      $loginUser->getPassword());
        }

        return $user;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    private function handleLogin (Request $request): Response {
        $loginForm = $this->createForm(LoginForm::class);
        $registrationForm = $this->createForm(RegistrationForm::class);

        $loginForm->handleRequest($request);
        $registrationForm->handleRequest($request);
        $message = null;

        try {
            $authRequest = $this->humble->validate($request);

            $user = $this->login($loginForm);
            if (!$user) {
                $user = $this->registration($registrationForm);
            }

            if ($user !== null) {
                return $this->humble->complete($authRequest, $user);
            }
        } catch (OAuthServerException $e) {
            $message = $e->getMessage();
        } catch (UniqueConstraintViolationException $e) {
            $message = 'user already exists!';
        }

        return $this->render('login.html.twig',
                             ['login_form' => $loginForm->createView(),
                              'registration_form' => $registrationForm->createView(),
                              'message' => $message,
                              'google_uri' => $this->generateUrl('google', $request->query->all()),
                              'facebook_uri' => $this->generateUrl('facebook', $request->query->all()),
                              'github_uri' => $this->generateUrl('github', $request->query->all())]);
    }

    /**
     * @param FormInterface $registrationForm
     *
     * @return User|null
     */
    private function registration (FormInterface $registrationForm): ?User {
        $user = null;
        if ($registrationForm->isSubmitted() && $registrationForm->isValid()) {
            /**@var $user User */
            $user = $registrationForm->getData();
            $user->savePassword($user->getPassword());
            $user = $this->userRepository->createUser($user);
        }

        return $user;
    }
}
