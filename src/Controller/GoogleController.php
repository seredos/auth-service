<?php


namespace App\Controller;

use App\Humble\GoogleAuthorizationHumble;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class GoogleController extends AbstractProviderController {
    public function __construct (SessionInterface $session,
                                 GoogleAuthorizationHumble $humble) {
        parent::__construct($session, $humble);
    }
}
