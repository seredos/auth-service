<?php


namespace App\Controller;


use App\Humble\AuthorizationHumbleInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

abstract class AbstractProviderController extends AbstractController {
    private $session;
    private $humble;

    public function __construct (SessionInterface $session,
                                 AuthorizationHumbleInterface $humble) {
        $this->session = $session;
        $this->humble = $humble;
    }

    public function authorization (Request $request): Response {
        $this->session->set('oauth_client_id', $request->query->get('client_id'));
        $this->session->set('oauth_redirect_uri', $request->query->get('redirect_uri'));
        $this->session->set('oauth_scope', $request->query->get('scope'));
        $this->session->set('oauth_state', $request->query->get('state'));
        $this->session->set('oauth_code_challenge', $request->query->get('code_challenge'));
        $this->session->set('oauth_code_challenge_method', $request->query->get('code_challenge_method'));

        return $this->humble->response($request);
    }

    public function visualize (Request $request): Response {
        $request->query->set('response_type', 'code');
        $request->query->set('client_id', $this->session->get('oauth_client_id'));
        $request->query->set('redirect_uri', $this->session->get('oauth_redirect_uri'));
        $request->query->set('scope', $this->session->get('oauth_scope'));
        $request->query->set('state', $this->session->get('oauth_state'));
        $request->query->set('code_challenge', $this->session->get('oauth_code_challenge'));
        $request->query->set('code_challenge_method', $this->session->get('oauth_code_challenge_method'));

        $this->session->remove('oauth_client_id');
        $this->session->remove('oauth_redirect_uri');
        $this->session->remove('oauth_scope');
        $this->session->remove('oauth_state');
        $this->session->remove('oauth_code_challenge');
        $this->session->remove('oauth_code_challenge_method');

        $authRequest = $this->humble->validate($request);

        return $this->humble->complete($authRequest);
    }
}
