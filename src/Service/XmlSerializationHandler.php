<?php


namespace App\Service;


class XmlSerializationHandler extends JsonSerializationHandler {
    protected const ACCEPT_CONTENT_TYPES = ['application/xml'];
    protected const TYPE                 = 'xml';
    protected const CONTENT_TYPE         = 'application/xml';
}
