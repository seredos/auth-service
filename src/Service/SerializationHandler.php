<?php


namespace App\Service;


use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

abstract class SerializationHandler implements SerializationHandlerInterface {
    private $successor;

    public function __construct (SerializationHandler $handler = null) {
        $this->successor = $handler;
    }

    final public function serialize (RequestInterface $request, $data, int $code = 200): ?ResponseInterface {
        $result = $this->processingSerialize($request, $data);

        if (!$result && $this->successor) {
            $result = $this->successor->serialize($request, $data);
        }

        return $result;
    }

    abstract protected function processingSerialize (RequestInterface $request, $data): ?ResponseInterface;
}
