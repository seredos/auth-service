<?php


namespace App\Service;


use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

interface SerializationHandlerInterface {
    public function serialize (RequestInterface $request, $data, int $code = 200): ?ResponseInterface;
}
