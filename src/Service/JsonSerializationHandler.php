<?php


namespace App\Service;


use JMS\Serializer\SerializerInterface;
use Nyholm\Psr7\Response;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class JsonSerializationHandler extends SerializationHandler {
    protected const ACCEPT_CONTENT_TYPES = [];
    protected const TYPE                 = 'json';
    protected const CONTENT_TYPE         = 'application/json';

    private $serializer;

    public function __construct (SerializerInterface $serializer, SerializationHandler $handler = null) {
        parent::__construct($handler);
        $this->serializer = $serializer;
    }

    protected function processingSerialize (RequestInterface $request, $data, int $code = 200): ?ResponseInterface {
        $result = null;
        if (0 === count($this::ACCEPT_CONTENT_TYPES) ||
            in_array($request->getHeader('accept')[0], $this::ACCEPT_CONTENT_TYPES, true)) {
            $content = $this->serializer->serialize($data, $this::TYPE);
            $result = new Response($code, ['Content-Type' => $this::CONTENT_TYPE], $content);
        }

        return $result;
    }
}
