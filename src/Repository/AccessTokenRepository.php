<?php


namespace App\Repository;


use App\Entity\AccessToken;
use League\OAuth2\Server\Entities\AccessTokenEntityInterface;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use League\OAuth2\Server\Repositories\AccessTokenRepositoryInterface;

class AccessTokenRepository implements AccessTokenRepositoryInterface {

    public function getNewToken (ClientEntityInterface $clientEntity, array $scopes, $userIdentifier = null) {
        $accessToken = new AccessToken();
        $accessToken->setClient($clientEntity);
        foreach ($scopes as $scope) {
            $accessToken->addScope($scope);
        }
        $accessToken->setUserIdentifier($userIdentifier);

        return $accessToken;
    }

    public function persistNewAccessToken (AccessTokenEntityInterface $accessTokenEntity) {
        // TODO: Implement persistNewAccessToken() method.
    }

    public function revokeAccessToken ($tokenId) {
        // TODO: Implement revokeAccessToken() method.
    }

    public function isAccessTokenRevoked ($tokenId) {
        return false;
    }
}
