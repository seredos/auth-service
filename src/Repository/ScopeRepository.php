<?php


namespace App\Repository;


use App\Entity\Scope;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\ScopeEntityInterface;
use League\OAuth2\Server\Repositories\ScopeRepositoryInterface;

/**
 * @method Scope|null find($id, $lockMode = null, $lockVersion = null)
 * @method Scope|null findOneBy(array $criteria, array $orderBy = null)
 * @method Scope[]    findAll()
 * @method Scope[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ScopeRepository extends ServiceEntityRepository implements ScopeRepositoryInterface {
    public function __construct (ManagerRegistry $registry) {
        parent::__construct($registry, Scope::class);
    }

    public function getScopeEntityByIdentifier ($identifier) {
        $scope = new Scope();
        $scope->setIdentifier($identifier);

        return $scope;
    }

    public function finalizeScopes (array $scopes,
                                    $grantType,
                                    ClientEntityInterface $clientEntity,
                                    $userIdentifier = null) {
        if ((int) $userIdentifier === 1) {
            $scope = new Scope();
            $scope->setIdentifier('core');
            $scopes[] = $scope;
        }

        return $scopes;
    }
}
