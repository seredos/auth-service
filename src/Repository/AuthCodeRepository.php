<?php


namespace App\Repository;


use App\Entity\AuthCode;
use League\OAuth2\Server\Entities\AuthCodeEntityInterface;
use League\OAuth2\Server\Exception\UniqueTokenIdentifierConstraintViolationException;
use League\OAuth2\Server\Repositories\AuthCodeRepositoryInterface;

class AuthCodeRepository implements AuthCodeRepositoryInterface {

    public function getNewAuthCode () {
        return new AuthCode();
    }

    public function persistNewAuthCode (AuthCodeEntityInterface $authCodeEntity) {
        // TODO: Implement persistNewAuthCode() method.
    }

    public function revokeAuthCode ($codeId) {
        // TODO: Implement revokeAuthCode() method.
    }

    public function isAuthCodeRevoked ($codeId) {
        return false;
    }
}
