<?php


namespace App\Repository;


use App\Entity\RefreshToken;
use League\OAuth2\Server\Entities\RefreshTokenEntityInterface;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;

class RefreshTokenRepository implements RefreshTokenRepositoryInterface {

    public function getNewRefreshToken () {
        return new RefreshToken();
    }

    public function persistNewRefreshToken (RefreshTokenEntityInterface $refreshTokenEntity) {
        // TODO: Implement persistNewRefreshToken() method.
    }

    public function revokeRefreshToken ($tokenId) {
        // TODO: Implement revokeRefreshToken() method.
    }

    public function isRefreshTokenRevoked ($tokenId) {
        // TODO: Implement isRefreshTokenRevoked() method.
    }
}
