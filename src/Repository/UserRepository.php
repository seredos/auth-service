<?php

namespace App\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Client\Provider\GoogleUser;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository implements UserRepositoryInterface {
    public function __construct (ManagerRegistry $registry) {
        parent::__construct($registry, User::class);
    }

    public function createUser (User $user): User {
        $this->getEntityManager()
             ->persist($user);
        $this->getEntityManager()
             ->flush();

        return $user;
    }

    public function getUserEntityByUserCredentials ($username,
                                                    $password,
                                                    $grantType,
                                                    ClientEntityInterface $clientEntity): ?User {
        return $this->getUserEntityByCredentials($username, $password);
    }

    public function getUserEntityByCredentials (string $username, string $password): ?User {
        $user = $this->findOneBy(['email' => $username]);
        if ($user !== null && !password_verify($password, $user->getPassword())) {
            $user = null;
        }

        return $user;
    }

    public function getUserEntityByGoogleId (string $token,
                                             GoogleUser $googleUser,
                                             ClientEntityInterface $clientEntity) {
        $user = $this->findOneBy(['googleId' => $googleUser->getId()]);
        if ($user === null) {
            $user = new User();
            $user->setGoogleId($googleUser->getId())
                 ->setGoogleToken($token)
                 ->setLastname($googleUser->getLastName())
                 ->setFirstname($googleUser->getFirstName())
                 ->setEmail($googleUser->getEmail());
        }
        $user->setGoogleToken($token);
        return $this->createUser($user);
    }
}
