<?php


namespace App\Repository;


use App\Entity\Client;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use League\OAuth2\Server\Repositories\ClientRepositoryInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

/**
 * @method Client|null find($id, $lockMode = null, $lockVersion = null)
 * @method Client|null findOneBy(array $criteria, array $orderBy = null)
 * @method Client[]    findAll()
 * @method Client[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ClientRepository extends ServiceEntityRepository implements ClientRepositoryInterface {
    private $router;

    public function __construct (ManagerRegistry $registry, UrlGeneratorInterface $router) {
        parent::__construct($registry, Client::class);
        $this->router = $router;
    }

    public function getClientEntity ($clientIdentifier) {
        $client = $this->findOneBy(['identifier' => $clientIdentifier]);

        if ($client === null) {
            $client = new Client();

            $client->setIdentifier($clientIdentifier);
            $client->setName($clientIdentifier);
            $client->setConfidential(false);

            $this->getEntityManager()
                 ->persist($client);
            $this->getEntityManager()
                 ->flush();
        }

        if (!$client->getRedirectUri()) {
            $client->setRedirectUri($this->router->generate('code_default_redirect',
                                                            [],
                                                            UrlGeneratorInterface::ABSOLUTE_URL));
        }

        return $client;
    }

    public function validateClient ($clientIdentifier, $clientSecret, $grantType) {
        $client = $this->findOneBy(['identifier' => $clientIdentifier]);
        if ($client === null) {
            return false;
        }

        $secret = $client->getSecret();
        if ($secret === null && $client->isConfidential()) {
            return false;
        }

        if ($secret !== null && $clientSecret !== $secret) {
            return false;
        }

        // TODO: validate grantType

        return true;
    }
}
