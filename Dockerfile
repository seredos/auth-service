ARG CI_PROJECT_NAMESPACE=seredos
ARG CI_PROJECT_NAME=auth-service
ARG CI_REGISTRY=registry.gitlab.com
ARG IMAGE_TAG=1.0.0
FROM $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/php-prod:$IMAGE_TAG AS auth-service-php

COPY . /builds/seredos/auth-service/

RUN chown -R www-data:www-data /builds/seredos/auth-service

FROM $CI_REGISTRY/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME/nginx:$IMAGE_TAG AS auth-service-web

COPY ./public/ /builds/seredos/auth-service/public/
