<?php

namespace App\Tests;

use App\DataFixtures\ClientFixtures;
use App\DataFixtures\UserFixtures;

class AuthorizationControllerTest extends WebTestCase {
    public function test_code_withoutType (): void {
        $client = static::createClient();
        $client->request('GET', '/en/');

        self::assertEquals(400,
                           $client->getResponse()
                                  ->getStatusCode());
    }

    public function test_code_withoutClient (): void {
        $client = static::createClient();
        $client->request('GET', '/en/?response_type=code');

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('div.alert-danger',
                                         'The authorization grant type is not supported by the authorization server.');
    }

    public function test_code_withoutScope (): void {
        $client = static::createClient();
        $client->request('GET', '/en/?response_type=code&client_id=untrusted');

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('div.alert-danger',
                                         'The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed.');
    }

    public function test_code_untrustedWithoutChallenge (): void {
        $client = static::createClient();
        $client->request('GET', '/en/?response_type=code&client_id=untrusted&scope=core');

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('div.alert-danger',
                                         'The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed.');
    }

    public function test_code_untrustedWithInvalidChallenge (): void {
        $client = static::createClient();
        $challenge = '12345';
        $client->request('GET', '/en/?response_type=code&client_id=untrusted&scope=core&code_challenge='.$challenge);

        self::assertResponseIsSuccessful();
        self::assertSelectorTextContains('div.alert-danger',
                                         'The request is missing a required parameter, includes an invalid parameter value, includes a parameter more than once, or is otherwise malformed.');
    }

    public function test_code_untrustedLogin (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures(), new UserFixtures()]);

        $challenge = bin2hex(random_bytes(24));
        $url = '/en/?response_type=code&client_id=untrusted&scope=core&code_challenge='.$challenge;
        $crawler = $client->request('GET', $url);

        self::assertResponseIsSuccessful();
        self::assertSelectorNotExists('div.alert-danger');

        $form = $crawler->filterXPath('//form[@name="login_form"]')
                        ->form([
                                   'login_form[email]' => 'test@web.de',
                                   'login_form[password]' => 'test'
                               ]);

        $client->submit($form);
        $client->followRedirect();
        $request = $client->getRequest();

        self::assertEquals('/en/redirect', $request->getPathInfo());
        self::assertTrue($request->query->has('code'));
        self::assertTrue(strlen($request->query->get('code')) > 128);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'authorization_code',
                          'client_id' => 'untrusted',
                          'code' => $request->query->get('code'),
                          'code_verifier' => $challenge]);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertEquals('Bearer', $response['token_type']);
        self::assertEquals('3600', $response['expires_in']);
        self::assertTrue(isset($response['access_token']) && strlen($response['access_token']) > 128);
        self::assertTrue(isset($response['refresh_token']) && strlen($response['refresh_token']) > 128);
    }

    public function test_code_untrustedRegistration (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures()]);

        $challenge = bin2hex(random_bytes(24));
        $url = '/en/?response_type=code&client_id=untrusted&scope=core&code_challenge='.$challenge;
        $crawler = $client->request('GET', $url);

        self::assertResponseIsSuccessful();
        self::assertSelectorNotExists('div.alert-danger');

        $form = $crawler->filterXPath('//form[@name="registration_form"]')
                        ->form([
                                   'registration_form[email]' => 'test@web.de',
                                   'registration_form[firstname]' => 'Max',
                                   'registration_form[lastname]' => 'Mustermann',
                                   'registration_form[password][first]' => 'P@ssw0rd',
                                   'registration_form[password][second]' => 'P@ssw0rd'
                               ]);

        $client->submit($form);
        $client->followRedirect();
        $request = $client->getRequest();

        self::assertEquals('/en/redirect', $request->getPathInfo());
        self::assertTrue($request->query->has('code'));
        self::assertTrue(strlen($request->query->get('code')) > 128);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'authorization_code',
                          'client_id' => 'untrusted',
                          'code' => $request->query->get('code'),
                          'code_verifier' => $challenge]);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertEquals('Bearer', $response['token_type']);
        self::assertEquals('3600', $response['expires_in']);
        self::assertTrue(isset($response['access_token']) && strlen($response['access_token']) > 128);
        self::assertTrue(isset($response['refresh_token']) && strlen($response['refresh_token']) > 128);
    }

    public function test_code_untrustedRegistration_withInvalidChallenge (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures()]);

        $challenge = bin2hex(random_bytes(24));
        $url = '/en/?response_type=code&client_id=untrusted&scope=core&code_challenge='.$challenge;
        $crawler = $client->request('GET', $url);

        self::assertResponseIsSuccessful();
        self::assertSelectorNotExists('div.alert-danger');

        $form = $crawler->filterXPath('//form[@name="registration_form"]')
                        ->form([
                                   'registration_form[email]' => 'test@web.de',
                                   'registration_form[firstname]' => 'Max',
                                   'registration_form[lastname]' => 'Mustermann',
                                   'registration_form[password][first]' => 'P@ssw0rd',
                                   'registration_form[password][second]' => 'P@ssw0rd'
                               ]);

        $client->submit($form);
        $client->followRedirect();
        $request = $client->getRequest();

        self::assertEquals('/en/redirect', $request->getPathInfo());
        self::assertTrue($request->query->has('code'));
        self::assertTrue(strlen($request->query->get('code')) > 128);
        $challenge = bin2hex(random_bytes(24));
        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'authorization_code',
                          'client_id' => 'untrusted',
                          'code' => $request->query->get('code'),
                          'code_verifier' => $challenge]);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertArrayNotHasKey('token_type', $response);
        self::assertArrayNotHasKey('access_token', $response);
        self::assertEquals(400, $response['code']);
    }

    public function test_code_trustedRegistration_withoutSecretInClient (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures()]);

        $url = '/en/?response_type=code&client_id=trusted&scope=core';
        $crawler = $client->request('GET', $url);

        self::assertResponseIsSuccessful();
        self::assertSelectorNotExists('div.alert-danger');

        $form = $crawler->filterXPath('//form[@name="registration_form"]')
                        ->form([
                                   'registration_form[email]' => 'test@web.de',
                                   'registration_form[firstname]' => 'Max',
                                   'registration_form[lastname]' => 'Mustermann',
                                   'registration_form[password][first]' => 'P@ssw0rd',
                                   'registration_form[password][second]' => 'P@ssw0rd'
                               ]);

        $client->submit($form);
        $client->followRedirect();
        $request = $client->getRequest();

        self::assertEquals('/en/redirect', $request->getPathInfo());
        self::assertTrue($request->query->has('code'));
        self::assertTrue(strlen($request->query->get('code')) > 128);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'authorization_code',
                          'client_id' => 'trusted',
                          'code' => $request->query->get('code')]);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertArrayNotHasKey('token_type', $response);
        self::assertArrayNotHasKey('access_token', $response);
        self::assertEquals(401, $response['code']);
    }

    public function test_code_trustedRegistration_withoutSecret (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures()]);

        $url = '/en/?response_type=code&client_id=trustedWithSecret&scope=core';
        $crawler = $client->request('GET', $url);

        self::assertResponseIsSuccessful();
        self::assertSelectorNotExists('div.alert-danger');

        $form = $crawler->filterXPath('//form[@name="registration_form"]')
                        ->form([
                                   'registration_form[email]' => 'test@web.de',
                                   'registration_form[firstname]' => 'Max',
                                   'registration_form[lastname]' => 'Mustermann',
                                   'registration_form[password][first]' => 'P@ssw0rd',
                                   'registration_form[password][second]' => 'P@ssw0rd'
                               ]);

        $client->submit($form);
        $client->followRedirect();
        $request = $client->getRequest();

        self::assertEquals('/en/redirect', $request->getPathInfo());
        self::assertTrue($request->query->has('code'));
        self::assertTrue(strlen($request->query->get('code')) > 128);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'authorization_code',
                          'client_id' => 'trustedWithSecret',
                          'code' => $request->query->get('code')]);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);
        self::assertArrayNotHasKey('token_type', $response);
        self::assertArrayNotHasKey('access_token', $response);
        self::assertEquals(401, $response['code']);
    }

    public function test_code_trustedRegistration_withSecret (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures()]);

        $url = '/en/?response_type=code&client_id=trustedWithSecret&scope=core';
        $crawler = $client->request('GET', $url);

        self::assertResponseIsSuccessful();
        self::assertSelectorNotExists('div.alert-danger');

        $form = $crawler->filterXPath('//form[@name="registration_form"]')
                        ->form([
                                   'registration_form[email]' => 'test@web.de',
                                   'registration_form[firstname]' => 'Max',
                                   'registration_form[lastname]' => 'Mustermann',
                                   'registration_form[password][first]' => 'P@ssw0rd',
                                   'registration_form[password][second]' => 'P@ssw0rd'
                               ]);

        $client->submit($form);
        $client->followRedirect();
        $request = $client->getRequest();

        self::assertEquals('/en/redirect', $request->getPathInfo());
        self::assertTrue($request->query->has('code'));
        self::assertTrue(strlen($request->query->get('code')) > 128);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'authorization_code',
                          'client_id' => 'trustedWithSecret',
                          'code' => $request->query->get('code'),
                          'client_secret' => 'secretSecret']);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertEquals('Bearer', $response['token_type']);
        self::assertEquals('3600', $response['expires_in']);
        self::assertTrue(isset($response['access_token']) && strlen($response['access_token']) > 128);
        self::assertTrue(isset($response['refresh_token']) && strlen($response['refresh_token']) > 128);
    }

    public function test_client_withoutClient (): void {
        $client = static::createClient();
        $client->request('POST', '/en/', ['grant_type' => 'client_credentials']);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertArrayNotHasKey('token_type', $response);
        self::assertArrayNotHasKey('access_token', $response);
        self::assertEquals(400, $response['code']);
    }

    public function test_client_untrusted (): void {
        $client = static::createClient();
        $client->request('POST', '/en/', ['grant_type' => 'client_credentials', 'client_id' => 'untrusted']);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertArrayNotHasKey('token_type', $response);
        self::assertArrayNotHasKey('access_token', $response);
        self::assertEquals(401, $response['code']);
    }

    public function test_client_trusted (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures()]);

        $client->request('POST', '/en/', ['grant_type' => 'client_credentials', 'client_id' => 'trusted']);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertArrayNotHasKey('token_type', $response);
        self::assertArrayNotHasKey('access_token', $response);
        self::assertEquals(401, $response['code']);
    }

    public function test_client_trusted_withScope (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures()]);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'client_credentials', 'client_id' => 'trusted', 'scope' => 'core']);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertArrayNotHasKey('token_type', $response);
        self::assertArrayNotHasKey('access_token', $response);
        self::assertEquals(401, $response['code']);
    }

    public function test_client_trusted_withoutSecret (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures()]);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'client_credentials', 'client_id' => 'trustedWithSecret', 'scope' => 'core']);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertArrayNotHasKey('token_type', $response);
        self::assertArrayNotHasKey('access_token', $response);
        self::assertEquals(401, $response['code']);
    }

    public function test_client_trusted_withSecret (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures()]);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'client_credentials',
                          'client_id' => 'trustedWithSecret',
                          'scope' => 'core',
                          'client_secret' => 'secretSecret']);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertArrayNotHasKey('refresh_token', $response);
        self::assertEquals('Bearer', $response['token_type']);
        self::assertEquals('3600', $response['expires_in']);
        self::assertTrue(isset($response['access_token']) && strlen($response['access_token']) > 128);
    }

    public function test_password_trusted (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures(), new UserFixtures()]);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'password',
                          'client_id' => 'trusted',
                          'scope' => 'core',
                          'username' => 'test@web.de',
                          'password' => 'test']);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertArrayNotHasKey('token_type', $response);
        self::assertArrayNotHasKey('access_token', $response);
        self::assertEquals(401, $response['code']);
    }

    public function test_password_trusted_withSecret (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures(), new UserFixtures()]);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'password',
                          'client_id' => 'trustedWithSecret',
                          'scope' => 'core',
                          'client_secret' => 'secretSecret',
                          'username' => 'test@web.de',
                          'password' => 'test']);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertEquals('Bearer', $response['token_type']);
        self::assertEquals('3600', $response['expires_in']);
        self::assertTrue(isset($response['access_token']) && strlen($response['access_token']) > 128);
        self::assertTrue(isset($response['refresh_token']) && strlen($response['refresh_token']) > 128);
    }

    public function test_password_trusted_withSecret_invalidPassword (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures(), new UserFixtures()]);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'password',
                          'client_id' => 'trustedWithSecret',
                          'scope' => 'core',
                          'client_secret' => 'secretSecret',
                          'username' => 'test@web.de',
                          'password' => 'test2']);

        $response = json_decode($client->getResponse()
                                       ->getContent(),
                                true);

        self::assertArrayNotHasKey('token_type', $response);
        self::assertArrayNotHasKey('access_token', $response);
        self::assertEquals(400, $response['code']);
    }

    public function test_code_refresh_untrustedLogin (): void {
        $client = static::createClient();

        $this->runFixtures($client->getContainer(), [new ClientFixtures(), new UserFixtures()]);

        $challenge = bin2hex(random_bytes(24));
        $url = '/en/?response_type=code&client_id=untrusted&scope=core&code_challenge='.$challenge;
        $crawler = $client->request('GET', $url);

        self::assertResponseIsSuccessful();
        self::assertSelectorNotExists('div.alert-danger');

        $form = $crawler->filterXPath('//form[@name="login_form"]')
                        ->form([
                                   'login_form[email]' => 'test@web.de',
                                   'login_form[password]' => 'test'
                               ]);

        $client->submit($form);
        $client->followRedirect();
        $request = $client->getRequest();

        self::assertEquals('/en/redirect', $request->getPathInfo());
        self::assertTrue($request->query->has('code'));
        self::assertTrue(strlen($request->query->get('code')) > 128);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'authorization_code',
                          'client_id' => 'untrusted',
                          'code' => $request->query->get('code'),
                          'code_verifier' => $challenge]);

        $firstResponse = json_decode($client->getResponse()
                                            ->getContent(),
                                     true);

        self::assertEquals('Bearer', $firstResponse['token_type']);
        self::assertEquals('3600', $firstResponse['expires_in']);
        self::assertTrue(isset($firstResponse['access_token']) && strlen($firstResponse['access_token']) > 128);
        self::assertTrue(isset($firstResponse['refresh_token']) && strlen($firstResponse['refresh_token']) > 128);

        $client->request('POST',
                         '/en/',
                         ['grant_type' => 'refresh_token',
                          'refresh_token' => $firstResponse['refresh_token'],
                          'client_id' => 'untrusted']);

        $secondResponse = json_decode($client->getResponse()
                                             ->getContent(),
                                      true);

        self::assertEquals('Bearer', $secondResponse['token_type']);
        self::assertEquals('3600', $secondResponse['expires_in']);
        self::assertTrue(isset($secondResponse['access_token']) && strlen($secondResponse['access_token']) > 128);
        self::assertTrue(isset($secondResponse['refresh_token']) && strlen($secondResponse['refresh_token']) > 128);
        self::assertNotEquals($firstResponse['access_token'], $secondResponse['access_token']);
        self::assertNotEquals($firstResponse['refresh_token'], $secondResponse['refresh_token']);
    }
}
