<?php


namespace App\Tests;


use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase as BaseWebTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

class WebTestCase extends BaseWebTestCase {
    /**
     * @var ContainerAwareLoader
     */
    private $fixtureLoader;

    protected function runFixtures (ContainerInterface $container, array $fixtures) {
        $this->initFixtures($container);
        foreach ($fixtures as $fixture) {
            $this->addFixture($fixture);
        }
        $this->executeFixtures($container->get('doctrine')
                                         ->getManager());
    }

    private function initFixtures (ContainerInterface $container) {
        $this->fixtureLoader = new ContainerAwareLoader($container);
    }

    private function addFixture (FixtureInterface $fixture) {
        $this->fixtureLoader->addFixture($fixture);
    }

    private function executeFixtures (EntityManager $manager) {
        $executor = new ORMExecutor($manager, new ORMPurger($manager));
        $executor->execute($this->fixtureLoader->getFixtures());
    }
}
